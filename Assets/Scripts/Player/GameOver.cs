using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using StarterAssets;


namespace Rubampeg.Lantern
{
    public class GameOver : MonoBehaviour
    {
        [SerializeField] private Image m_FadeImage;
        [SerializeField] private float m_FadeDuration = 2f;
        [SerializeField] private AnimationCurve m_FadingCurve;
        [Space]
        [SerializeField] private SoundManager m_SoundManager;
        [SerializeField] private FirstPersonController m_Controller;


        public void SnuffOut()
        {
            m_Controller.enabled = false;
            m_SoundManager.PlaySound(Sound.blow);
            m_FadeImage.color = new Color(0, 0, 0, 1);
            StartCoroutine(GameOverCoroutine());

            //StartCoroutine(FadeToBlack());
        }

        private void RestartGame()
        {
            SceneManager.LoadScene("MainMap");
        }

        private IEnumerator FadeToBlack()
        {
            Color InitialColor = new Color(0f, 0f, 0f, 0f);
            Color TargetColor = new Color(InitialColor.r, InitialColor.g, InitialColor.b, 1f);

            float ElapsedTime = 0f;

            while (ElapsedTime < m_FadeDuration)
            {
                yield return null;
                ElapsedTime += Time.deltaTime;

                float Step = m_FadingCurve.Evaluate(ElapsedTime / m_FadeDuration);
                m_FadeImage.color = Color.Lerp(InitialColor, TargetColor, Step);
            }

            RestartGame();
        }

        private IEnumerator GameOverCoroutine() {
            yield return new WaitForSeconds(2f);
            RestartGame();
        }
    }
}
