using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StarterAssets;
using UnityEngine.InputSystem;

namespace Rubampeg
{
    public class WakeUpBehaviour : MonoBehaviour

    {
        [SerializeField]
        private float m_delay;

        [SerializeField]
        private Image m_fadeImage;

        [SerializeField]
        private float m_fadeDuration;

        [SerializeField]
        private float m_cameraDuration;

        [SerializeField]
        private AnimationCurve m_cameraCurve;

        [SerializeField]
        private AnimationCurve m_fadeCurve;

        [SerializeField]
        private FirstPersonController m_controller;

        void Start()
        {
            m_controller.enabled = false;
            StartCoroutine(ReduceAlpha());
            StartCoroutine(MoveCamera());
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        IEnumerator ReduceAlpha() {
            yield return new WaitForSeconds(m_delay);
            GetComponent<SoundManager>().PlaySound(Sound.gasp);
            Color _initialColor = m_fadeImage.color;
            Color _targetColor = new Color(_initialColor.r, _initialColor.g, _initialColor.b, 0);

            float _elapsedTime = 0f;

            while (_elapsedTime < m_fadeDuration) {
                _elapsedTime += Time.deltaTime;
                float _step = m_fadeCurve.Evaluate(_elapsedTime / m_fadeDuration);
                m_fadeImage.color = Color.Lerp(_initialColor, _targetColor, _step);
                yield return null;
            }
        }

        IEnumerator MoveCamera() {
            yield return new WaitForSeconds(m_delay);
            Quaternion _initialRotation = transform.rotation;
            Quaternion _targetRotation = Quaternion.Euler(0f, 180f, 0f);

            float _elapsedTime = 0f;

            while (_elapsedTime < m_fadeDuration) {
                _elapsedTime += Time.deltaTime;
                float _step = m_cameraCurve.Evaluate(_elapsedTime / m_cameraDuration);
                transform.rotation = Quaternion.Lerp(_initialRotation, _targetRotation, _step);
                yield return null;
            }

            m_controller.enabled = true;
        }
    }
}
