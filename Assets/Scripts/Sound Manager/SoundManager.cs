using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sound
{   gasp,
    blow,
}

[System.Serializable]
public struct SoundAudioClip
{
    public Sound sound;
    public AudioClip audioClip;
    
}

public class SoundManager : MonoBehaviour
{
    public SoundAudioClip[] soundAudioClipArray;
    public AudioSource sfxAudioSource;
    public AudioClip[] stepsArray;

    private List<int> steps = new List<int>();
    private bool isStepPlaying = false;

    public void PlaySound(Sound sound) 
    {
        GameObject audioGO = new GameObject("Sound");
        AudioSource audioSource =  audioGO.AddComponent<AudioSource>();
        audioSource.PlayOneShot(GetAudioClip(sound));
        StartCoroutine(WaitUntilSoundEnds(audioGO, audioSource));
    }   

    private AudioClip GetAudioClip(Sound sound) {
        foreach (SoundAudioClip soundAudioClip in soundAudioClipArray) {
            if (soundAudioClip.sound == sound) {
                return soundAudioClip.audioClip;
            }
        }
        Debug.LogError("Sound " + sound + " not found!");
        return null;
    }

    public void PlaySteps() {
        if (!isStepPlaying) {
            isStepPlaying = true;
            if (steps.Count == 9) {
                steps.Clear();
            }
            int _stepIndex = 0;
            while (steps.Contains(_stepIndex)) {
                _stepIndex = Random.Range(0, 9);
            }
            steps.Add(_stepIndex);
            GameObject audioGO = new GameObject("Step");
            AudioSource audioSource = audioGO.AddComponent<AudioSource>();
            audioSource.volume = 0.5f;
            audioSource.PlayOneShot(stepsArray[_stepIndex]);
            StartCoroutine(WaitUntilyStepEnds(audioGO, audioSource));
        }
    }

    private IEnumerator WaitUntilSoundEnds(GameObject audio, AudioSource audioSource) {

        yield return new WaitUntil(() => !audioSource.isPlaying);
        Destroy(audio);
    }
    
    private IEnumerator WaitUntilyStepEnds(GameObject audio, AudioSource audioSource) {
        yield return new WaitUntil(() => !audioSource.isPlaying);
        isStepPlaying = false;
        Destroy(audio);
    }


}
