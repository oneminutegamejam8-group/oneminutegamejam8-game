using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRiddle
{
    void Solve();
    bool IsSolved();
    bool CheckSolved(); //return true if was just solved
}
