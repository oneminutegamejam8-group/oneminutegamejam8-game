using UnityEngine;
using UnityEngine.InputSystem;

namespace Rubampeg
{
    public class FireplaceRiddle : MonoBehaviour, IRiddle
    {
        [SerializeField] private GameObject m_Fireplace;
        [SerializeField] private GameObject m_LensRay;
        [SerializeField] private GameObject m_LensRayLight;
        [SerializeField] private GameObject m_FireplaceInteractionPoint;
        [SerializeField] private SecretMessageBehaviour m_SecretMessage;
        [SerializeField] private CollectInteractable m_Lens;
        [Space]
        [SerializeField] private Transform m_PlayerCamera;

        public bool AreLensInPlace
        {
            get => m_Lens.b_isPlaced;
        }

        private bool m_bSolved = false;

        public void Solve()
        {
            m_bSolved = true;
            m_Fireplace.SetActive(true);
            m_SecretMessage.ShowSecretMessage();
        }

        public bool IsSolved() => m_bSolved;

        public bool CheckSolved()
        {
            throw new System.NotImplementedException();
        }


        private void OnTriggerStay(Collider other)
        {
            if (AreLensInPlace && other.CompareTag("Player"))
            {
                Vector3 playerToTarget = (m_FireplaceInteractionPoint.transform.position - m_PlayerCamera.position).normalized;

                Debug.DrawRay(m_FireplaceInteractionPoint.transform.position, playerToTarget);
                Debug.DrawRay(m_PlayerCamera.position, m_PlayerCamera.forward);

                if (Mouse.current.rightButton.IsPressed() && Vector3.Dot(m_PlayerCamera.forward, playerToTarget) > 1f - 0.001f)
                {
                    if (!IsSolved()) Solve();
                    m_LensRay.SetActive(true);
                    m_LensRayLight.SetActive(true);
                }
                else
                {
                    m_LensRay.SetActive(false);
                    m_LensRayLight.SetActive(false);
                }
            }
        }
    }
}
