using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Rubampeg
{
    public class PointLightRiddle : MonoBehaviour, IRiddle
    {
        [SerializeField] private GameObject m_lookAt;
        [SerializeField] private float m_toleranceDot = .2f;
        [SerializeField] private Texture m_requiredMask;
        [SerializeField] private Light m_normalPumpkinLight;
        [SerializeField] private Light m_focusedPumpkinLight;


        [SerializeField] private GameObject[] m_riddleObjects;
        [SerializeField] private GameObject[] m_ObjectsToActivate;
        [SerializeField] private bool m_bIsPumpkin = false;
        [SerializeField] private Texture m_pumpkinMask;

        bool m_solved = false;
        [SerializeField] private AudioSource m_audioSource;
        [SerializeField] private GameObject m_limb;
        [Space]
        [SerializeField] private float m_LookDuration = 1f;

        private float m_CurrentTime = 0f;

        private static int m_remainingRiddles = 2;

        public bool CheckSolved()
        {
            throw new System.NotImplementedException();
        }

        public bool IsSolved()
        {
            return m_solved;
        }

        public void Solve()
        {
            Debug.Log("SOLVED POINT LIGHT RIDDLE");
            m_solved = true;

            foreach (GameObject ToActivate in m_ObjectsToActivate)
            {
                ToActivate.SetActive(true);
            }
        }

        void OnTriggerStay(Collider other)
        {
            if(other.CompareTag("Player"))
            {
                Vector3 playerToTarget = (m_lookAt.transform.position - other.transform.position).normalized;

                if (Mouse.current.rightButton.IsPressed() && Vector3.Dot(other.transform.forward, playerToTarget) > 1 - m_toleranceDot
                    && m_requiredMask == m_focusedPumpkinLight.cookie)
                {
                    while (m_CurrentTime < m_LookDuration)
                    {
                        m_CurrentTime += Time.deltaTime;
                        return;
                    }

                    if(m_bIsPumpkin)
                    {
                        m_remainingRiddles = 2;

                        foreach (var r in m_riddleObjects)
                        {
                            r.SetActive(true);

                            foreach (GameObject ToActivate in m_ObjectsToActivate)
                            {
                                ToActivate.SetActive(true);
                            }
                        }
                    }
                    else
                    {
                        Solve();
                        
                        m_remainingRiddles--;
                        if(m_remainingRiddles == 0)
                        {
                            foreach (var r in m_riddleObjects)
                            {
                                r.SetActive(false);
                            }
                            m_focusedPumpkinLight.cookie = m_pumpkinMask;
                            m_normalPumpkinLight.cookie = m_pumpkinMask;
                            m_limb.SetActive(true);
                        }
                    }
                    m_audioSource.Play();
                    this.gameObject.SetActive(false);
                    m_lookAt.SetActive(false);
                }
                else
                {
                    m_CurrentTime = 0f;
                }
            }
        }
    }
}
