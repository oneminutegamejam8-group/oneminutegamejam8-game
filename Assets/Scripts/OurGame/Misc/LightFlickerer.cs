using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Rubampeg.Miscellaneous
{
    [RequireComponent(typeof(Light))]
    public class LightFlickerer : MonoBehaviour
    {
        [SerializeField] private AnimationCurve m_FlickerCurve;
        [SerializeField] private float m_SingleLoopDuration = 5f;

        private float m_CurrentTime = 0f;

        private Light c_Light;
        private float c_DefaultIntensity;

        private void Awake()
        {
            c_Light = GetComponent<Light>();
            c_DefaultIntensity = c_Light.intensity;
        }

        private void Update()
        {
            if (m_CurrentTime < m_SingleLoopDuration)
            {
                float CurrentStep = m_FlickerCurve.Evaluate(m_CurrentTime / m_SingleLoopDuration);
                c_Light.intensity = c_DefaultIntensity * CurrentStep;

                m_CurrentTime += Time.deltaTime;
            }
            else
            {
                c_Light.intensity = c_DefaultIntensity * m_FlickerCurve.Evaluate(1f);

                m_CurrentTime = 0f;
            }
        }


        private void OnValidate()
        {
            if (m_SingleLoopDuration < 0.1f) m_SingleLoopDuration = 0.1f;
        }
    }
}
