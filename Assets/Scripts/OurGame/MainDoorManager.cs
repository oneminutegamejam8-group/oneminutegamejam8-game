using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

namespace Rubampeg
{
    public class MainDoorManager : MonoBehaviour
    {
        [SerializeField] private PadLockInteractable[] m_locks;

        //private bool m_bCanInteract = true;
        public static MainDoorManager instance { get; private set; }
        private int index = 0;
        private int m_locksRemaining;

        void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
        }

        void Start()
        {
            m_locksRemaining = m_locks.Length;
        }

        public void AddKey()
        {
            Debug.Assert(index < m_locks.Length);
            m_locks[index++].SetCanInteract(true);
        }

        public void LockRemoved()
        {
            m_locksRemaining--;
            if(m_locksRemaining == 0)
            {
                //END GAME
                Debug.Log("END GAME");
                Debug.Assert(false);
            }
        }

    }
}
