using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Rubampeg
{
    public class BookshelfBehaviour : MonoBehaviour
    {
        [SerializeField]
        private float m_movementTime;

        [SerializeField]
        private AudioClip m_audio;

        [SerializeField]
        private AnimationCurve m_movementCurve;

        public void StartMove()
        {
            StartCoroutine(Move());
            GetComponent<AudioSource>().PlayOneShot(m_audio);
        }

        public IEnumerator Move() {
            Vector3 _initialPosition = transform.position;
            Vector3 _targetPosition = new Vector3(transform.position.x, transform.position.y -4.4f, transform.position.z);

            float _elapsedTime = 0f;

            while (_elapsedTime < m_movementTime) {
                _elapsedTime += Time.deltaTime;
                float _step = m_movementCurve.Evaluate(_elapsedTime / m_movementTime);
                transform.position = Vector3.Lerp(_initialPosition, _targetPosition, _step);
                yield return null;
            }

 
        }
    }
}
