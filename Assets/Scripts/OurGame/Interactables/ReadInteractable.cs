using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using StarterAssets;
using UnityEngine.InputSystem;

namespace Rubampeg
{
    public class ReadInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private string m_name;

        [SerializeField]
        private Image m_image;

        [SerializeField]
        private Sprite m_sprite;

        [SerializeField]
        private float m_width;

        [SerializeField]
        private float m_height;

        [SerializeField]
        private string m_description;

        [SerializeField]
        private FirstPersonController m_controller;

        [SerializeField]
        private AudioClip m_soundClip;


        [SerializeField] private GameObject m_interactionObj;
        [SerializeField] private GameObject m_interactionImg;
        [SerializeField] private TextMeshProUGUI m_interactionText;
        
        private bool m_isOpen = false;

        public bool CanInteract()
        {
            return true;
        }

        public string GetName()
        {
            return m_name;
        }

        public void Interact()
        {
            if (!m_isOpen)
            {
                m_interactionText.text = m_description;
                m_interactionImg.SetActive(true);
                m_interactionObj.SetActive(true);
                m_controller.enabled = false;
                m_isOpen = true;
                GetComponent<AudioSource>().PlayOneShot(m_soundClip);
            }
            else
            {
                m_interactionText.text = "";
                m_interactionImg.SetActive(false);
                m_interactionObj.SetActive(false);
                m_controller.enabled = true;
                m_isOpen = false;
            }
        }

        public void SetCanInteract(bool canInteract)
        {
            throw new System.NotImplementedException();
        }

        // Update is called once per frame
        void Update()
        {
            /*if (m_isOpen && Keyboard.current.eKey.wasPressedThisFrame) {
                m_interactionCanvas.gameObject.SetActive(false);
                m_controller.gameObject.SetActive(true);
                m_isOpen = false;
            }*/
        }
    }
}
