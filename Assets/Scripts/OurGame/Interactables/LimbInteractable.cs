using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class LimbInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private string m_name;
        [SerializeField] private AudioClip m_clip;

        public bool CanInteract()
        {
            return true;
        }

        public string GetName()
        {
            return m_name;
        }

        public void Interact()
        {
            GetComponent<AudioSource>().PlayOneShot(m_clip);
            Inventory.instance.m_limbTags.Add(gameObject.tag);
            Destroy(gameObject);
        }

        public void SetCanInteract(bool canInteract)
        {
            
        }
    }
}
