using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class PadLockInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private GameObject m_key;
        [SerializeField] private AudioSource m_audioSource;

        private bool m_HasKey = false;

        public bool CanInteract()
        {
            return true;
        }

        public string GetName()
        {
            return m_HasKey ? "Unlock" : "Locked";
        }

        public void Interact()
        {
            if(m_HasKey)
            {
                //sound effect
                MainDoorManager.instance.LockRemoved();
                Destroy(m_key);
                Destroy(this.gameObject);
            }
        }

        public void SetCanInteract(bool canInteract)
        {
            m_key.SetActive(canInteract);
            m_HasKey = canInteract;
        }
    }
}
