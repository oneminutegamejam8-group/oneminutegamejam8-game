using UnityEngine;


namespace Rubampeg.Lantern
{
    public class PickableLanternInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private GameObject m_PlayerLantern;
        [SerializeField] private AudioClip m_clip;

        private bool m_bInteractable = true;

        public void Interact()
        {
            GetComponent<AudioSource>().PlayOneShot(m_clip);
            m_PlayerLantern.SetActive(true);
            gameObject.SetActive(false);
        }

        public bool CanInteract() => m_bInteractable;
        public void SetCanInteract(bool canInteract) => m_bInteractable = canInteract;

        public string GetName() => "Pumpkin Head";
    }
}
