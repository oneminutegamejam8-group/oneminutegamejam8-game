using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockChangeHandInteractable : MonoBehaviour, IInteractable
{
    [SerializeField] private GameObject m_hoursHand;
    [SerializeField] private GameObject m_minutesHand;

    [SerializeField] private ClockHandInteractable m_handInteractableAdd;
    [SerializeField] private ClockHandInteractable m_handInteractableRemove;

    private bool m_hours = true;
    private bool m_canInteract = true;

    public void Interact()
    {
        if(m_hours)
        {
            m_handInteractableAdd.m_currentHand = m_minutesHand;
            m_handInteractableRemove.m_currentHand = m_minutesHand;
        }
        else
        {
            m_handInteractableAdd.m_currentHand = m_hoursHand;
            m_handInteractableRemove.m_currentHand = m_hoursHand;
        }
        
        m_hours = !m_hours;
    }

    public bool CanInteract()
    {
        return m_canInteract;
    }

    public string GetName()
    {
        return "Change Hours/Minutes";
    }

    public void SetCanInteract(bool canInteract)
    {
        m_canInteract = canInteract;
    }
}
