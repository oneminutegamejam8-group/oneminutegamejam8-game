using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockHandInteractable : MonoBehaviour, IInteractable, IRiddle
{
    [SerializeField] public GameObject m_currentHand;
    [SerializeField] private float m_degrees;
    [Space]
    [SerializeField, Range(0, 11)] private int m_solveHours = 3;
    [SerializeField, Range(0, 11)] private int m_solveMinutes = 9; //solves at 3:45
    [Space]
    bool m_bSolved = false;
    [SerializeField] public GameObject m_minutesHand;
    [SerializeField] public GameObject m_hoursHand;
    [SerializeField] public GameObject m_rootClock;

    [SerializeField] private GameObject m_limbToSpawn;

    [SerializeField] private AudioClip m_clipSolved;
    [SerializeField] private AudioClip m_clipInteraction;

    public void Interact()
    {
        GetComponent<AudioSource>().PlayOneShot(m_clipInteraction);
        m_currentHand.transform.localEulerAngles = new Vector3(
            m_currentHand.transform.localEulerAngles.x,
            m_currentHand.transform.localEulerAngles.y + m_degrees,
            m_currentHand.transform.localEulerAngles.z
            );

        if (CheckSolved())
            Solve();
    }

    public bool CanInteract()
    {
        return !m_bSolved;
    }

    public string GetName()
    {
        return m_degrees > 0 ? $"Decrement {m_currentHand.name}" : $"Increment {m_currentHand.name}";
    }
    public void SetCanInteract(bool canInteract)
    {
        m_bSolved = !canInteract;
    }

    public void Solve()
    {
        Debug.Log("SOLVED");
        m_bSolved = true;
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().PlayOneShot(m_clipSolved);


        IInteractable[] interactables = m_rootClock.GetComponentsInChildren<IInteractable>();
        
        foreach(var i in interactables)
        {
            i.SetCanInteract(false);
        }
        m_limbToSpawn.SetActive(true);
    }

    public bool IsSolved()
    {
        return m_bSolved;
    }

    public bool CheckSolved()
    {
        float MinutesDegrees = m_minutesHand.transform.localEulerAngles.y;
        float HoursDegrees = m_hoursHand.transform.localEulerAngles.y;

        return AreEqual(Mathf.DeltaAngle(MinutesDegrees, m_solveMinutes * m_degrees), 0f) &&
            AreEqual(Mathf.DeltaAngle(HoursDegrees, m_solveHours * m_degrees), 0f);
    }

    private bool AreEqual(float a, float b)
    {
        return a > b - Mathf.Epsilon && a < b + Mathf.Epsilon;
    }
}