using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class LightCookieInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private string m_text;
        [SerializeField] private Light m_mainLight;
        [SerializeField] private Light m_focusedLight;
        [SerializeField] private Texture m_cookie;

        [SerializeField] private GameObject[] m_otherPapers;


        public bool CanInteract()
        {
            return true;
        }

        public string GetName()
        {
            return m_text;
        }

        public void Interact()
        {
            m_mainLight.cookie = m_cookie;
            m_focusedLight.cookie = m_cookie;

            foreach(GameObject g in m_otherPapers)
            {
                g.SetActive(true);
            }

            this.gameObject.SetActive(false);
        }

        public void SetCanInteract(bool canInteract)
        {
            throw new System.NotImplementedException();
        }
    }
}
