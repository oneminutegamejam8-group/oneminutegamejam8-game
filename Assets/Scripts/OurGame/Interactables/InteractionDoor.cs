using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InteractionDoor : MonoBehaviour, IInteractable
{
    [SerializeField]
    private bool b_isOpen = false;
    [SerializeField]
    private float m_rotationSpeed = 50f;
    [SerializeField] private Transform m_Joint;

    [SerializeField]
    private AudioClip m_clip;

    Coroutine m_currentCoroutine;
    bool m_canInteract = true;

    public void Interact()
    {
        if (m_currentCoroutine != null)
        {
            StopCoroutine(m_currentCoroutine);
        }
        m_currentCoroutine = StartCoroutine(MoveDoor(b_isOpen));
    }

    public bool CanInteract()
    {
        return m_canInteract;
    }

    public string GetName()
    {
        return "Door";
    }

    private IEnumerator MoveDoor(bool opened)
    {
        GetComponent<AudioSource>().PlayOneShot(m_clip);
        if (!opened)
        {
            b_isOpen = true;
            while (transform.localEulerAngles.y >= 270 || transform.localEulerAngles.y == 0) // < 0
            {
                transform.RotateAround(m_Joint.transform.position, Vector3.up, -m_rotationSpeed * Time.deltaTime);
                yield return null;
            }
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 270, transform.localEulerAngles.z);
        }
        else
        {
            b_isOpen = false;
            while (transform.localEulerAngles.y >= 270)
            {
                transform.RotateAround(m_Joint.transform.position, Vector3.up, m_rotationSpeed * Time.deltaTime);
                yield return null;
            }
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 0, transform.localEulerAngles.z);
        }
        m_currentCoroutine = null;
    }

    public void SetCanInteract(bool canInteract)
    {
        m_canInteract = canInteract;
    }
}
