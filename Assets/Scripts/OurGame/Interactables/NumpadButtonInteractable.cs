using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class NumpadButtonInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] TMPro.TextMeshPro m_text;
        [SerializeField] int m_buttonValue;
        [SerializeField] int m_maxDigits;

        private bool m_bCanInteract = true;

        public bool CanInteract()
        {
            return m_bCanInteract;
        }

        public string GetName()
        {
            return m_buttonValue.ToString();
        }

        public void Interact()
        {
            if(m_text.text.Length < m_maxDigits)
            {
                m_text.text += m_buttonValue.ToString();
            }
        }

        public void SetCanInteract(bool canInteract)
        {
            m_bCanInteract =  canInteract;
        }
    }
}
