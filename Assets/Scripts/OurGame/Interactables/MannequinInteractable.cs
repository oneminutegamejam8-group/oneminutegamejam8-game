using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using StarterAssets;
using UnityEngine.SceneManagement;

namespace Rubampeg
{
    public class MannequinInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private GameObject m_rightArm;
        [SerializeField] private GameObject m_leftArm;
        [SerializeField] private GameObject m_rightLeg;
        [SerializeField] private GameObject m_leftLeg;
        [SerializeField] private AudioClip m_clip;

        [SerializeField] private GameObject m_player;
        [SerializeField] private Transform m_endingPosition;
        [SerializeField] private GameObject m_playerCameraRoot;
        [SerializeField] private GameObject m_endPumpkinHead;
        [SerializeField] private GameObject m_playerPumpkin;
        [SerializeField] private AudioClip endingMusic;
        [SerializeField] private AudioClip endingBlow;
        [SerializeField] private AudioClip endingCreak;
        [SerializeField] private AudioClip endingLaugh;
        [SerializeField] private UnityEngine.UI.Image interactionDot;
        [SerializeField] private UnityEngine.UI.Image theEndLogo;

        int m_numLimbs = 0;
        bool m_bCanInteract = true;

        public bool CanInteract()
        {
            return m_bCanInteract;
        }

        public string GetName()
        {
            if (m_numLimbs == 4)
                return "Place head";

            int numLimbs = 0;
            foreach (var t in Inventory.instance.m_limbTags)
            {
                switch (t)
                {
                    case "Right Arm":
                    case "Left Arm":
                    case "Right Leg":
                    case "Left Leg":
                        numLimbs++;
                        break;
                }
            }
            return $"Add limbs - {numLimbs}";
        }

        public void Interact()
        {
            if (m_numLimbs == 4)
            {
                AudioSource[] _sources = m_player.GetComponentsInChildren<AudioSource>();

                foreach(AudioSource source in _sources) {
                    Destroy(source);
                }

                AudioSource endingAudioSource = gameObject.AddComponent<AudioSource>();
                endingAudioSource.PlayOneShot(endingMusic);
                m_player.GetComponent<FirstPersonController>().enabled = false;
                interactionDot.gameObject.SetActive(false);
                m_player.transform.position = new Vector3(m_endingPosition.position.x, m_endingPosition.position.y - 1.5f, m_endingPosition.position.z);
                m_player.transform.rotation = m_endingPosition.rotation;
                m_playerCameraRoot.transform.rotation = m_endingPosition.rotation;
                m_endPumpkinHead.SetActive(true);
                m_bCanInteract = false;
                GetComponent<Outline>().enabled = false;
                m_playerPumpkin.SetActive(false);
                StartCoroutine(EndAnimation(endingAudioSource));
            }


            bool breakLoop = false;
            foreach(var t in Inventory.instance.m_limbTags)
            {
                GetComponent<AudioSource>().PlayOneShot(m_clip);
                switch (t)
                {
                    case "Right Arm":
                        m_rightArm.SetActive(true);
                        Inventory.instance.m_limbTags.Remove(t);
                        m_numLimbs++;
                        breakLoop = true;
                        break;
                    case "Left Arm":
                        m_leftArm.SetActive(true);
                        Inventory.instance.m_limbTags.Remove(t);
                        m_numLimbs++;
                        breakLoop = true;
                        break;
                    case "Right Leg":
                        m_rightLeg.SetActive(true);
                        Inventory.instance.m_limbTags.Remove(t);
                        m_numLimbs++;
                        breakLoop = true;
                        break;
                    case "Left Leg":
                        m_leftLeg.SetActive(true);
                        Inventory.instance.m_limbTags.Remove(t);
                        m_numLimbs++;
                        breakLoop = true;
                        break;
                }
                if (breakLoop) break;
            }
        }

        public void SetCanInteract(bool canInteract)
        {
            throw new System.NotImplementedException();
        }

        [SerializeField] private float m_animationTime = 5f;
        [SerializeField] private float m_animationMovement = 2f;
        [SerializeField] private float m_upOffset = 1f;
        [SerializeField] private UnityEngine.UI.Image m_blackScreen;
        public IEnumerator EndAnimation(AudioSource source)
        {
            float m_currentTime = 0;

            Vector3 startPose = m_player.transform.position;
            Vector3 endPose = m_player.transform.position + m_player.transform.forward * m_animationMovement;
            while (m_currentTime < m_animationTime)
            {
                m_player.transform.position = Vector3.Lerp(startPose, endPose, m_currentTime / m_animationTime) + Vector3.up * m_upOffset;
                yield return null;
                m_currentTime += Time.deltaTime;
            }
            m_player.transform.position = endPose + Vector3.up * m_upOffset;
            source.Stop();
            yield return new WaitForSeconds(3f);

            source.PlayOneShot(endingBlow);
            m_blackScreen.color = new Color(0, 0, 0, 1);
            yield return new WaitForSeconds(3f);
            theEndLogo.gameObject.SetActive(true);
            source.PlayOneShot(endingLaugh);
            yield return new WaitForSeconds(7f);

            SceneManager.LoadScene("Menu");
        }
    }

    
}
