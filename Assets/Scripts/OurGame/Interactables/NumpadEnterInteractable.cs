using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rubampeg
{
    public class NumpadEnterInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] TMPro.TextMeshPro m_text;
        [SerializeField] int m_accessCode;
        [SerializeField] GameObject m_buttons;
        [SerializeField] AudioClip m_accessGrantedClip;
        [SerializeField] AudioClip m_errorClip;
        [SerializeField] UnityEvent m_accessGrantedEvent;

        private bool m_bCanInteract = true;
        private AudioSource m_audioSource;

        void Start()
        {
            m_audioSource = GetComponentInParent<AudioSource>();
        }

        public bool CanInteract()
        {
            return m_bCanInteract;
        }

        public string GetName()
        {
            return "Enter";
        }

        public void Interact()
        {
            int number;
            if(int.TryParse(m_text.text, out number))
            {
                if(number == m_accessCode)
                {
                    //ACCESS GRANTED
                    //PLAY SOUND
                    Debug.Log("ACCESS GRANTED");
                    m_audioSource.clip = m_accessGrantedClip;
                    m_audioSource.Play();
                    m_accessGrantedEvent.Invoke();
                    m_buttons.SetActive(false);
                }
                else
                {
                    m_text.text = "";
                    //PLAY ERROR SOUND
                    m_audioSource.clip = m_errorClip;
                    m_audioSource.Play();
                    Debug.Log("Error");
                }
            }
        }

        public void SetCanInteract(bool canInteract)
        {
            m_bCanInteract = canInteract;
        }

    }
}
