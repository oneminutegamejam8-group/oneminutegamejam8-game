using UnityEngine;


namespace Rubampeg
{
    public class CollectInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private string m_name;

        [SerializeField]
        private Inventory m_playerInventory;

        [SerializeField] private AudioClip m_clip;

        public bool b_isPlaced = false;

        public bool CanInteract() {
            return true;
        }

        public string GetName() {
            return m_name;
        }

        public void Interact()
        {
            b_isPlaced = false;
            GetComponent<AudioSource>().PlayOneShot(m_clip);
            m_playerInventory.Add(gameObject);
            gameObject.SetActive(false);
        }

        public void SetCanInteract(bool canInteract) {
            throw new System.NotImplementedException();
        }
    }
}
