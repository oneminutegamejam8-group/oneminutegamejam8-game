using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class CompositeInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private string m_name;

        [SerializeField]
        private GameObject m_itemNeededForInteraction;

        [SerializeField]
        private Vector3 m_itemNeededPosition;

        [SerializeField]
        private Vector3 m_itemNeededRotation;

        private Quaternion m_itemNeededQuaternion;

        [SerializeField]
        private Inventory m_playerInventory;

        [SerializeField]
        private AudioClip m_clip;

        public bool CanInteract() {
            return true;
        }

        public string GetName() {
            return m_name;
        }

        public void Interact() {
            
            if (m_playerInventory.m_inventoryList.Contains(m_itemNeededForInteraction))
            {
                Place(m_itemNeededForInteraction);
                GetComponent<AudioSource>().PlayOneShot(m_clip);
                m_playerInventory.m_inventoryList.Remove(m_itemNeededForInteraction);

                m_itemNeededForInteraction.GetComponent<CollectInteractable>().b_isPlaced = true;
            }
        }

        public void SetCanInteract(bool canInteract) {
            throw new System.NotImplementedException();
        }

        void Start()
        {
            m_itemNeededQuaternion = Quaternion.Euler(m_itemNeededRotation);
        }

        private void Place(GameObject item) {
            item.transform.localPosition = m_itemNeededPosition;
            item.transform.rotation = m_itemNeededQuaternion;
            item.SetActive(true);
        }
    }
}
