using UnityEngine;

namespace Rubampeg
{
    public class CandleInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private bool m_IsLit = true;
        [SerializeField] private GameObject m_Light;

        private bool m_bInteractable = true;

        private void TurnLight(bool bOn)
        {
            m_Light.SetActive(bOn);
        }

        private void Awake()
        {
            TurnLight(m_IsLit);
        }

        public void Interact()
        {
            if (CanInteract())
            {
                m_IsLit = !m_IsLit;

                TurnLight(m_IsLit);
            }
        }

        public bool CanInteract() => m_bInteractable;
        public void SetCanInteract(bool canInteract) => m_bInteractable = canInteract;

        public string GetName() => "Candle";
    }
}
