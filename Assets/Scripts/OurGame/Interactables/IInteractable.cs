public interface IInteractable
{
    public void Interact();
    public bool CanInteract();
    public string GetName();
    public void SetCanInteract(bool canInteract);
}
