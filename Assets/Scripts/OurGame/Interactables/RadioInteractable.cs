using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class RadioInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private GameObject m_frequencyIndicator;
        [SerializeField] private float m_offsetFrequencies = .3f;
        [SerializeField] private int m_solutionFrequency = 3;
        [SerializeField] private AudioClip m_noiseClip;
        [SerializeField] private AudioClip m_morseClip;
        [SerializeField] private AudioClip m_songClip;

        private AudioSource m_audioSource;
        private Vector3[] m_indicatorPosition = new Vector3[4];
        private bool m_bSolved = false;

        void Start()
        {
            m_audioSource = GetComponent<AudioSource>();

            for (int i = 0; i < 4; i++)
            {
                m_indicatorPosition[i] = m_frequencyIndicator.transform.position + m_frequencyIndicator.transform.forward * m_offsetFrequencies * i;
            }
        }

        public bool CanInteract()
        {
            return !m_bSolved;
        }

        public string GetName()
        {
            return "Radio";
        }

        private int index = 0;
        public void Interact()
        {
            index = ++index % 4;
            m_frequencyIndicator.transform.position = m_indicatorPosition[index];
            if (index == m_solutionFrequency)
            {
                m_audioSource.clip = m_morseClip;
                m_audioSource.Play();
            }
            else if(index == (m_solutionFrequency + 1) % 4)
            {
                m_audioSource.clip = m_noiseClip;
                m_audioSource.Play();
            }
            else if (index == 1) {
                m_audioSource.clip = m_songClip;
            }
        }

        public void SetCanInteract(bool canInteract)
        {
            m_bSolved = canInteract;
        }
    }
}
