using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class InteractionManager : MonoBehaviour
{
    [SerializeField] float m_interactionDistance = 5f;
    [SerializeField] TMPro.TextMeshProUGUI m_text;
    [SerializeField] UI_InteractionDot m_dot;
    [SerializeField] RenderTexture m_renderTexture;
    [SerializeField] RectTransform m_rectTrans; //render texture trans
    [SerializeField] Canvas m_canvasTransform;

    Outline m_lastObjOutline;
    Camera m_mainCamera;
    string m_baseText;
    float m_scaleFactor;


    void Start()
    {
        m_scaleFactor = m_canvasTransform.scaleFactor;
        m_mainCamera = Camera.main;
        m_baseText = m_text.text;
    }

    void Update()
    {
        RaycastHit _hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out _hit, m_interactionDistance))
        {
            if (m_lastObjOutline && _hit.transform != m_lastObjOutline.transform)
            {
                m_lastObjOutline.enabled = false;
            }

            IInteractable hitObject = _hit.transform.GetComponent<IInteractable>();
            if (hitObject != null && hitObject.CanInteract())
            {
                m_lastObjOutline = _hit.transform.GetComponent<Outline>();
                if (m_lastObjOutline != null)
                {
                    m_lastObjOutline.enabled = true;
                }

                /*m_text.transform.position = m_mainCamera.WorldToScreenPoint(_hit.transform.position);
                m_text.transform.position = ConvertWorldToScreen(_hit.transform.position);*/
                m_text.enabled = true;
                m_text.text = $"{m_baseText} ({hitObject.GetName()})";

                if(Keyboard.current.eKey.wasPressedThisFrame)
                {
                    hitObject.Interact();
                }
            }
            else
            {
                m_text.enabled = false;
            }


            m_dot.ChangeAlpha(1);

        }
        else
        {
            if (m_lastObjOutline) m_lastObjOutline.enabled = false;
            m_text.enabled = false;
            m_dot.ChangeAlpha(.5f);

        }
    }

    public Vector3 ConvertWorldToScreen(Vector3 positionIn)
    {
        Vector3 viewPos = m_mainCamera.WorldToViewportPoint(positionIn);
        Vector2 localPos = new Vector2(viewPos.x * m_rectTrans.rect.width, viewPos.y * m_rectTrans.rect.height);
        Vector3 worldPos = m_rectTrans.TransformPoint(localPos);
        float scalerRatio = ((1 / transform.lossyScale.x) * 2) * m_scaleFactor;
        return new Vector3(worldPos.x - m_rectTrans.rect.width / scalerRatio, worldPos.y - m_rectTrans.rect.height / scalerRatio, 1f);
    }
}
