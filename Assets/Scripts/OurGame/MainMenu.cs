using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Rubampeg
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private GameObject m_mainButtons;
        [SerializeField] private GameObject m_credits;
        [SerializeField] private UnityEngine.UI.Image m_fadeImage;
        [SerializeField] private AudioSource m_audioSource;


        private void Start() {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
        }
        public void Play()
        {
            StartCoroutine(FadeAudio(GetComponent<AudioSource>(), 3f, 0f));
            StartCoroutine(FadeToBlack(m_fadeImage, 3f));
        }

        public void Quit()
        {
            Application.Quit();
        }

        public void Credits()
        {
            m_credits.SetActive(true);
            m_mainButtons.SetActive(false);
        }

        public void EndCredits()
        {
            m_credits.SetActive(false);
            m_mainButtons.SetActive(true);
        }

        public static IEnumerator FadeAudio(AudioSource audioSource, float duration, float targetVolume) {
            float currentTime = 0;
            float start = audioSource.volume;
            while (currentTime < duration) {
                currentTime += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                yield return null;
            }
        }

        public static IEnumerator FadeToBlack(UnityEngine.UI.Image imageSource, float duration) {
            float currentTime = 0;
            Color startingColor = imageSource.color;
            Color targetColor = new Color(0, 0, 0, 1);
            imageSource.gameObject.SetActive(true);
            while (currentTime < duration) {
                currentTime += Time.deltaTime;
                imageSource.color = Color.Lerp(startingColor, targetColor, currentTime / duration);
                yield return null;
            }

            SceneManager.LoadScene("MainMap");
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        public void PlayClickSound() {
            m_audioSource.Play();
        }
    }
}
