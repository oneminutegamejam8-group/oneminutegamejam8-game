using UnityEngine;
using UnityEngine.Assertions;

namespace Rubampeg.Lantern
{
    /**
     * This class makes the lantern light point to a specific point of the world.
     * Usually, this will be placed centered on the player screen.
     */
    public class LanternLookUp : MonoBehaviour
    {
        [SerializeField] private Transform m_LookUpPoint = null;


        private void Awake()
        {
            Assert.IsNotNull(m_LookUpPoint);
        }

        private void Update()
        {
            transform.LookAt(m_LookUpPoint);
        }
    }
}
