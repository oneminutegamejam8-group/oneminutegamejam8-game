using System; // C# Actions
using UnityEngine;
using UnityEngine.Assertions;


namespace Rubampeg.Lantern
{
    [RequireComponent(typeof(Light))]
    public class Torch : MonoBehaviour
    {
        public float TorchLightLevel { get; set; }

        private AnimationCurve m_LightCurve = null;
        private float m_Lifetime;
        private bool m_bTicking = false;
        private float m_ElapsedLifeTime = 0f;

        private Light c_Light;
        private float c_InitialIntensity;


        public Action OnTorchDeath;


        private void Awake()
        {
            c_Light = GetComponent<Light>();
            c_InitialIntensity = c_Light.intensity;
        }

        private void Update()
        {
            if (m_bTicking)
            {
                m_ElapsedLifeTime += Time.deltaTime;

                float CurrentStep = m_LightCurve.Evaluate(m_ElapsedLifeTime / m_Lifetime);

                c_Light.intensity = Mathf.Lerp(0f, c_InitialIntensity, CurrentStep);
                c_Light.intensity *= TorchLightLevel;

                if (m_ElapsedLifeTime > m_Lifetime)
                {
                    m_bTicking = false;
                    OnTorchDeath?.Invoke();
                }
            }
        }


        public void Init(float Lifetime, AnimationCurve LightCurve)
        {
            Assert.IsTrue(Lifetime > 0f);

            m_Lifetime = Lifetime;
            m_LightCurve = LightCurve;
            m_bTicking = true;
        }

        private float Remap(float iMin, float iMax, float oMin, float oMax, float v)
        {
            float t = Mathf.InverseLerp(iMin, iMax, v);
            return Mathf.Lerp(oMin, oMax, t);
        }
    }
}
