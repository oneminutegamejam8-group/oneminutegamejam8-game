using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

namespace Rubampeg.Lantern
{
    public class LanternFocuser : MonoBehaviour
    {
        [SerializeField] private Transform m_BasePosition;
        [SerializeField] private Transform m_FocusedPosition;
        [Space]
        [SerializeField] private TorchController m_BaseLanternLight;
        [SerializeField] private TorchController m_FocusedLanternLight;
        [Space]
        [SerializeField] private float m_InterpolationTime = 0.75f;

        private IEnumerator c_CameraMoverCoroutine = null;


        private void Awake()
        {
            Assert.IsNotNull(m_BasePosition);
            Assert.IsNotNull(m_FocusedPosition);
            Assert.IsNotNull(m_BaseLanternLight);
            Assert.IsNotNull(m_FocusedLanternLight);
            transform.position = m_BasePosition.position;
        }

        private void Update()
        {
            if (Mouse.current.rightButton.wasPressedThisFrame)
            {
                if (c_CameraMoverCoroutine != null) StopCoroutine(c_CameraMoverCoroutine);

                c_CameraMoverCoroutine = CameraMover(m_BasePosition, m_FocusedPosition, m_BaseLanternLight, m_FocusedLanternLight);
                StartCoroutine(c_CameraMoverCoroutine);
            }
            else if (Mouse.current.rightButton.wasReleasedThisFrame)
            {
                if (c_CameraMoverCoroutine != null) StopCoroutine(c_CameraMoverCoroutine);

                c_CameraMoverCoroutine = CameraMover(m_FocusedPosition, m_BasePosition, m_FocusedLanternLight, m_BaseLanternLight);
                StartCoroutine(c_CameraMoverCoroutine);
            }
        }

        private IEnumerator CameraMover(Transform StartTransform, Transform EndTransform, TorchController StartLight, TorchController EndLight)
        {
            // We get the current step of the interpolation between Start and End,
            // and we use that step to get the remaining time before we get to
            // EndPoint, and gradually interpolating to there.

            float t = InverseLerp(StartTransform.position, EndTransform.position, transform.position);
            float CurrentTime = Mathf.Lerp(0f, m_InterpolationTime, t);

            while (CurrentTime < m_InterpolationTime)
            {
                yield return null;
                CurrentTime += Time.deltaTime;

                float InterpolationStep = CurrentTime / m_InterpolationTime;

                transform.position = Vector3.Lerp(transform.position, EndTransform.position, InterpolationStep);
                EndLight.SetTorchesLightLevel(InterpolationStep);
                StartLight.SetTorchesLightLevel(1f - InterpolationStep);
            }

            //transform.position = EndTransform.position;
            transform.SetParent(EndTransform);
            transform.localPosition = Vector3.zero;
            EndLight.SetTorchesLightLevel(1f);
            StartLight.SetTorchesLightLevel(0f);

            c_CameraMoverCoroutine = null;
        }


        private void OnValidate()
        {
            if (m_InterpolationTime < 0.1f) m_InterpolationTime = 0.1f;
        }

        public static float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
        {
            Vector3 AB = b - a;
            Vector3 AV = value - a;
            return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
        }
    }
}
