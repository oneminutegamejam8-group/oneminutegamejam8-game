using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


namespace Rubampeg.Lantern
{
    public class TorchController : MonoBehaviour
    {
        [SerializeField] private AnimationCurve m_LightFalloffCurve;
        [SerializeField] private float m_LightLifetime = 60f;
        [SerializeField] private bool m_StartsOn = true;
        [Space]
        [SerializeField] private GameOver m_GameOverResponder;
        [Space]
        [SerializeField] private List<Torch> m_Torches;

        private bool m_bTorchAreDead = false;

        public void SetTorchesLightLevel(float InterpolationStep)
        {
            InterpolationStep = Mathf.Clamp01(InterpolationStep);

            foreach (Torch torch in m_Torches)
            {
                torch.TorchLightLevel = InterpolationStep;
            }
        }


        private void Awake()
        {
            SetTorchesLightLevel(m_StartsOn ? 1f : 0f);

            foreach (Torch torch in m_Torches)
            {
                torch.Init(m_LightLifetime, m_LightFalloffCurve);
                torch.OnTorchDeath += RespondToTorchDeath;
            }
        }

        private void RespondToTorchDeath()
        {
            if (m_bTorchAreDead) return;
            m_bTorchAreDead = true;

            if (m_GameOverResponder != null)
            {
                m_GameOverResponder.SnuffOut();
            }
        }


        private void OnValidate()
        {
            if (m_LightLifetime < 0.1f) m_LightLifetime = 0.1f;
        }
    }
}