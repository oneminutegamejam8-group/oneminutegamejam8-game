using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_InteractionDot : MonoBehaviour
{
    [SerializeField]
    private Image m_DotImage;

    public float m_AlphaInteracting = 255;
    public float m_AlphaNotInteracting = 112;
    

    void Start()
    {
        m_DotImage = GetComponent<Image>();
    }

    public void ChangeAlpha(float _newAlpha) {
        Color _dotColor = m_DotImage.color;
        m_DotImage.color = new Color(_dotColor.r, _dotColor.g, _dotColor.b, _newAlpha);
    }
}
