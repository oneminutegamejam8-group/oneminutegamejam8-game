using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class MagnyfingGlassBehaviour : MonoBehaviour
    {

        private CollectInteractable m_collect;

        [SerializeField]
        private Light m_light;

        [SerializeField]
        private Transform m_lightBeam;

        [SerializeField] private FireplaceRiddle m_FireplaceRiddle;

        void Start()
        {
            m_collect = GetComponent<CollectInteractable>();
        }
    }
}
