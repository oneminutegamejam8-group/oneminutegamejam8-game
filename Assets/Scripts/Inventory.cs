using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rubampeg
{
    public class Inventory : MonoBehaviour
    {
        public List<GameObject> m_inventoryList;
        public List<string> m_limbTags;

        public static Inventory instance { get; private set; }

        void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
        }

        void Start()
        {
            m_inventoryList = new List<GameObject>();
        }


        public void Add(GameObject item) {
            m_inventoryList.Add(item);
        }

        public void Remove(GameObject item) {

            if (m_inventoryList.Contains(item)) {
                m_inventoryList.Remove(item);
            }
        }
    }
}
