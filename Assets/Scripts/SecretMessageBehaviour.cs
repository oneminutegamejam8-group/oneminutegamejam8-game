using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;

namespace Rubampeg
{
    public class SecretMessageBehaviour : MonoBehaviour
    {
        [SerializeField]
        private float m_fadeDuration;
        private TextMeshProUGUI m_text;

        [SerializeField]
        private AnimationCurve m_fadeCurve;

        void Start()
        {
            m_text = GetComponentInChildren<TextMeshProUGUI>();
            
        }

        public void ShowSecretMessage() { StartCoroutine(IncreaseAlpha()); }

        IEnumerator IncreaseAlpha() {
            Color _initialColor = m_text.color;
            Color _targetColor = new Color(_initialColor.r, _initialColor.g, _initialColor.b, 1);

            float _elapsedTime = 0f;

            while (_elapsedTime < m_fadeDuration) {
                _elapsedTime += Time.deltaTime;
                float _step = m_fadeCurve.Evaluate(_elapsedTime / m_fadeDuration);
                m_text.color = Color.Lerp(_initialColor, _targetColor, _step);
                yield return null;
            }            
        }
    }
}
